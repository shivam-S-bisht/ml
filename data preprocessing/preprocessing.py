import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


# importing the dataset
dataset = pd.read_csv('Data.csv')


# #converting categorical data variables to dummy variables
dataset = pd.get_dummies(data = dataset, columns=['Country', 'Purchased'])
print(dataset)


#splitting independent variables and dependent variables
X = dataset.iloc[:, :-2].values
y = dataset.iloc[:,5:7].values
# print(X)


#adding a missing data
from sklearn.impute import SimpleImputer
simpleimputer = SimpleImputer(missing_values=np.nan, strategy= 'mean', fill_value=0 )
X[:, :2] = simpleimputer.fit_transform(X[:, :2])
print(X)













# import pandas as pd
# import matplotlib.pyplot as plt
# import numpy as np

# #importing dataset
# dataset = pd.read_csv('Data.csv')
# X = dataset.iloc[:,:-1].values
# y = dataset.iloc[:,-1].values


# #splitting dataset into training and test set
# from sklearn.model_selection import train_test_split
# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)
# print(y_train, y_test)